/******************************************************************************
 * Technische Informatik 2                                                    *
 *                                                                            *
 * Exercise 8 - Task 1b                                                       *
 *                                                                            *
 * Student:                  Felix Rüdiger                                    *
 * Matrikel No.:             214002                                           *
 *                                                                            *
 * File:                     round_robin.c                                    *
 *                                                                            *
 * Original source location: https://gitlab.com/fruediger-ti2/round_robin     *
 *                                                                            *
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <math.h>

typedef unsigned long long int ullong_t;

// Queue node
typedef struct process_node_t process_node_t;
typedef struct process_node_t
{
    size_t          id;
    ullong_t        service_time;
    process_node_t *next;
} process_node_t;

// dequeue (take) an element from a queue (represented by it's head and tail nodes)
static inline void dequeue(process_node_t **head, process_node_t **tail, process_node_t **node)
{
    if (node != NULL)
    {
        if (*head != NULL)
        {
            if ((*head = (*node = *head)->next) == NULL)
                *tail = NULL;

            (*node)->next = NULL;
        }
        else
            *node = NULL;
    }
}

// enqueue (put) an element into a queue (represented by it's head and tail nodes)
static inline void enqueue(process_node_t **head, process_node_t **tail, process_node_t *node)
{
    if (*tail == NULL)
        *head = *tail = node;
    else
        *tail = ((*tail)->next = node);
    node->next = NULL;
}

// preform a round robin simulation
void round_robin(const ullong_t *service_times, ullong_t *finishing_times, size_t size, ullong_t time_quantum)
{
    process_node_t *head = NULL;
    process_node_t *tail = NULL;

    // "copy" the service times as nodes into a queue
    for (size_t n = 0; n < size; n++)
    {
        process_node_t *node = (process_node_t *)malloc(sizeof(process_node_t));

        node->id           = n;
        node->service_time = service_times[n];

        enqueue(&head, &tail, node);
    }

    ullong_t global_time = 0;

    // do as long as there are still processes to simulate
    while (head != NULL)
    {
        process_node_t *node;

        // get the next process
        dequeue(&head, &tail, &node);

        // could this process finished within the next time slice or does it need more ready times?
        if (node->service_time > time_quantum)
        {
            // this process needs at least one more iteration

            // increase the global time by the time quantum nad decrease the process' service time by the time quantum
            global_time        += time_quantum;
            node->service_time -= time_quantum;

            // put it back at the end of the queue
            enqueue(&head, &tail, node);
        }
        else
        {
            // this process finishes within the current time slice

            // increase the global time by the remaining time needed for the process to finish store it's finishing time into to output array
            global_time               += node->service_time;
            finishing_times[node->id]  = global_time;

            // this process is not needed any more, so it can be freed
            free(node);
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc < 2)
        exit(EXIT_SUCCESS);
    
    // parse the commandline arguments as input time services

    ullong_t *service_times = (ullong_t *)malloc(sizeof(ullong_t) * (argc - 1));

    size_t size = 0;
    for (size_t n = 1; n < argc; n++)
    {
        char *strend;
        ullong_t t = strtoull(argv[n], &strend, 0);

        if ((t == ULLONG_MAX) || (t == 0 && strend == argv[n]) || (*strend))
            printf("The given service time in position %zu (\"%s\") seems to be erroneous; skipping it.\n", n, argv[n]);
        else
            service_times[size++] = t;
    }

    if (size < argc - 1)
        service_times = (ullong_t *)realloc(service_times, sizeof(ullong_t) * size);

    ullong_t  max_service_time     = 0;
    ullong_t *finishing_times      = (ullong_t *)malloc(sizeof(ullong_t) * size);
    ullong_t *min_finishing_times  = (ullong_t *)malloc(sizeof(ullong_t) * size);
    ullong_t  min_avg_time_quantum = 0;
    float     min_avg_time         = INFINITY;

    /*
     * max_service_time     ... is the maximum given service time
     * services_times       ... is now the array of given service times
     *           size       ... is it's size in number of elements; this is also the number of processes to simulate
     * finishing_times      ... is a holder array to store the output of a single round robin simulation (it's size is obviously also size)
     * min_finishing_times  ... is an array to store the finishing times of processes from the round robin simulation with the minimum average waiting time
     * min_avg_time_qunatum ... is the time quantum of the round robin simulation with the minimum average waiting time
     * min_avg_time         ... is the minimum average waiting of all round robin simulations
     */

    // prepare the tables header
    char print_helper[5];

    printf("     Q");
    for (size_t n = 0; n < size; n++)
    {
        sprintf(print_helper, "P%zu", n + 1);
        printf("  %4s", print_helper);
    }
    printf("  avg. time\n");

    printf("------");
    for (size_t n = 0; n < size; n++)
    {
        printf("------");
    }
    printf("-----------\n");

    // find the maximum service time
    for (size_t n = 0; n < size; n++)
        if (service_times[n] > max_service_time)
            max_service_time = service_times[n];

    // simulation round robin with time quantums from 1 to the maximum given service time (in this configuration every process can finish within it's first time slice)
    for (ullong_t time_quantum = 1; time_quantum <= max_service_time; time_quantum++)
    {
        // simulate round robin
        round_robin(service_times, finishing_times, size, time_quantum);

        ullong_t sum_time = 0;

        // print out the calculated finishing times for each process
        printf("  %4llu", time_quantum);
        for (size_t n = 0; n < size; n++)
        {
            sum_time += finishing_times[n];
            printf("  %4llu", finishing_times[n]);
        }

        float avg_time = (float)sum_time / (float)size;

        // print out the average waiting time (the average of all finishing times)
        printf("  %8.2f\n", avg_time);

        // if we got a mimimum by the average waiting time, we store the current configuration as mimimum
        if (avg_time < min_avg_time)
        {
            min_avg_time_quantum = time_quantum;
            min_avg_time         = avg_time;
            memcpy(min_finishing_times, finishing_times, sizeof(ullong_t) * size);
        }
    }

    // prepare the tables header for the mimimum average waiting time configuration
    printf("\nMinimal average time configuration (first occurrence):\n");

    printf("     Q");
    for (size_t n = 0; n < size; n++)
    {
        sprintf(print_helper, "P%zu", n + 1);
        printf("  %4s", print_helper);
    }
    printf("  avg. time\n");

    printf("------");
    for (size_t n = 0; n < size; n++)
    {
        printf("------");
    }
    printf("-----------\n");


    // print out the minimum average waiting time configuration
    printf("  %4llu", min_avg_time_quantum);
    for (size_t n = 0; n < size; n++)
    {
        printf("  %4llu", min_finishing_times[n]);
    }
    printf("  %8.2f\n", min_avg_time);

    printf("\n");

    // clean up a bit
    free(service_times);
    free(finishing_times);
    free(min_finishing_times);
}